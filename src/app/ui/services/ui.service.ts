import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UiService {
  private sidebarCarShowsubject = new BehaviorSubject<boolean>(false);
  constructor() { }

  openSidebarCar() {
    this.sidebarCarShowsubject.next(true);
  }
  closeSidebarCar() {
    this.sidebarCarShowsubject.next(false);
  }
  getSidebarState(): Observable<boolean> {
    return this.sidebarCarShowsubject.asObservable();
  }

}
