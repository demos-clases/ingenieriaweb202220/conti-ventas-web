import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/app/store/data/models/product.model';
import { ProductCarService } from 'src/app/store/services/product-car.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ui-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.sass']
})
export class ProductCardComponent implements OnInit {
  @Input()
  product!: Product;
  name?: string;
  baseUrl = environment.storeApiUrl;
  constructor(
    private router: Router,
    private car: ProductCarService,
  ) { }

  ngOnInit(): void {
  }
  setProduct(){
    this.car.addProduct(this.product);
  }
}
