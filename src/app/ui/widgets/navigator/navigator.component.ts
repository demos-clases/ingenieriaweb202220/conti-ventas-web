import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/store/data/models/product.model';
import { ProductCarService } from 'src/app/store/services/product-car.service';
import { UiService } from '../../services/ui.service';

@Component({
  selector: 'ui-navigator',
  templateUrl: './navigator.component.html',
  styleUrls: ['./navigator.component.sass']
})
export class NavigatorComponent implements OnInit {
  quantity= 0;
  constructor(
    private ui: UiService,
    private car: ProductCarService,
  ) { }

  ngOnInit(): void {
    console.log('init nav');
    this.car.getItems().subscribe({
      next: (items) => {
        this.quantity = items.length
        console.log('nav', items);
      }
    });
  }


  openCarSidebar() {
    this.ui.openSidebarCar();
  }
}
