import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarCarComponent } from './sidebar-car.component';

describe('SidebarCarComponent', () => {
  let component: SidebarCarComponent;
  let fixture: ComponentFixture<SidebarCarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SidebarCarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarCarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
