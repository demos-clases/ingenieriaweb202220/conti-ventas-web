import { Component, OnInit } from '@angular/core';
import { ProductCarItem } from 'src/app/store/data/models/productCar.model';
import { ProductCarService } from 'src/app/store/services/product-car.service';
import { UiService } from '../../services/ui.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ui-sidebar-car',
  templateUrl: './sidebar-car.component.html',
  styleUrls: ['./sidebar-car.component.sass']
})
export class SidebarCarComponent implements OnInit {
  sidebarVisible = false;
  items: ProductCarItem[] = [];
  baseUrl = environment.storeApiUrl;
  constructor(
    private car: ProductCarService,
    private ui: UiService,
  ) { }

  ngOnInit(): void {
    this.car.getItems().subscribe({
      next: (items) => this.items = items
    });
    this.ui.getSidebarState().subscribe({
      next: (state) => this.sidebarVisible = state
    });
  }

  remove(productId: number): void {
    this.car.removeProduct(productId);
  }

  add(item:ProductCarItem, increment: number): void {
    item.quantity = +item.quantity
    // item.quantity = item.quantity + increment
    if (+item.quantity + increment < 1) {
      return;
    }
    item.quantity += increment;
    this.car.changeQuantity(item.product.id, item.quantity);
  }

  onKeypressEvent(e: any) {
    const limitKeysOf = ['0', '1', '2','3','4','5','6','7','8','9'];
    if (limitKeysOf.indexOf(e.key) == -1 ) {
      // console.log('evitar que escriba el key', e.key);
      e.preventDefault();
    }
  }

  total() {
    // version con reduce
    let fnSuma = (i: number, a: ProductCarItem) => {
      return  i +(a.product.precioVenta * a.quantity);
    }
    return this.items
      .reduce(fnSuma, 0);
    
      //version tradicional
    
    // let contador = 0;
    // for(let i = 0; i < this.items.length; i++) {
    //   contador = contador + (this.items[i].quantity * this.items[i].product.precioVenta );
    // }
    // return contador;
    
  }
}
