import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// PrimeNg Modules
import {ButtonModule} from 'primeng/button';
import {SidebarModule} from 'primeng/sidebar';
import {ToolbarModule} from 'primeng/toolbar';
import {CardModule} from 'primeng/card';
import { TagModule } from 'primeng/tag';

// custom
import { ProductCardComponent } from './widgets/product-card/product-card.component';
import { NavigatorComponent } from './widgets/navigator/navigator.component';
import { SidebarCarComponent } from './templates/sidebar-car/sidebar-car.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ProductCardComponent,
    NavigatorComponent,
    SidebarCarComponent
  ],
  imports: [
    CommonModule,
    ButtonModule,
    FormsModule,
    SidebarModule,
    ToolbarModule,
    CardModule,
    TagModule,
  ],
  exports: [
    // primeng
    ButtonModule,
    SidebarModule,
    ToolbarModule,
    CardModule,
    TagModule,
    // custom
    ProductCardComponent,
    NavigatorComponent,
    SidebarCarComponent,
  ]
})
export class UiModule { }
