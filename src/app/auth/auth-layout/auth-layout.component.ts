import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-auth-layout',
  templateUrl: './auth-layout.component.html',
  styleUrls: ['./auth-layout.component.sass']
})
export class AuthLayoutComponent implements OnInit {
  sidebarVisible = true;
  constructor() { }

  ngOnInit(): void {
    console.log('AuthLayoutComponent');
  }

}
