import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthLoginComponent } from './screens/auth-login/auth-login.component';
import { AuthRegisterComponent } from './screens/auth-register/auth-register.component';
import { AuthRecoveryComponent } from './screens/auth-recovery/auth-recovery.component';
import { AuthLayoutComponent } from './auth-layout/auth-layout.component';
import { AuthRoutingModule } from './auth-routing.module';
import { UiModule } from '../ui/ui.module';



@NgModule({
  declarations: [
    AuthLoginComponent,
    AuthRegisterComponent,
    AuthRecoveryComponent,
    AuthLayoutComponent
  ],
  imports: [
    CommonModule,
    UiModule,
    AuthRoutingModule,
  ]
})
export class AuthModule { }
