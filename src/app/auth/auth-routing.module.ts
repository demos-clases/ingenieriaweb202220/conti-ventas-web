import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthLayoutComponent } from './auth-layout/auth-layout.component';
import { AuthLoginComponent } from './screens/auth-login/auth-login.component';
import { AuthRecoveryComponent } from './screens/auth-recovery/auth-recovery.component';
import { AuthRegisterComponent } from './screens/auth-register/auth-register.component';

const routes: Routes = [
  {
    path: '', component: AuthLayoutComponent, 
    children: [
      {path: '', redirectTo: 'ingresar', pathMatch: 'full'},
      {path: 'ingresar', component: AuthLoginComponent},
      {path: 'registrarse', component: AuthRegisterComponent},
      {path: 'recuperar-contraseña', component: AuthRecoveryComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
