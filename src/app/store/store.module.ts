import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// custon components
import { StoreLayoutComponent } from './store-layout/store-layout.component';
import { StoreHomeComponent } from './screens/store-home/store-home.component';
import { StoreCategoryComponent } from './screens/store-category/store-category.component';
import { StoreProductComponent } from './screens/store-product/store-product.component';
import { StoreRoutingModule } from './store-routing.module';
import { UiModule } from '../ui/ui.module';



@NgModule({
  declarations: [
    StoreLayoutComponent,
    StoreHomeComponent,
    StoreCategoryComponent,
    StoreProductComponent
  ],
  imports: [
    CommonModule,
    UiModule,
    // own
    StoreRoutingModule,
  ]
})
export class StoreModule { }
