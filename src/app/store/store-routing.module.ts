import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StoreCategoryComponent } from './screens/store-category/store-category.component';
import { StoreHomeComponent } from './screens/store-home/store-home.component';
import { StoreProductComponent } from './screens/store-product/store-product.component';
import { StoreLayoutComponent } from './store-layout/store-layout.component';

const routes: Routes = [
  {
    path: '', component: StoreLayoutComponent, 
    children: [
      {path: '', component: StoreHomeComponent},
      {path: 'categoria/:categorySlug', component: StoreCategoryComponent},
      {path: 'producto/:productSlug', component: StoreProductComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StoreRoutingModule { }
