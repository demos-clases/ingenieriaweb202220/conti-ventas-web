import { Product } from "./product.model";

export interface ProductCarItem {
  product: Product;
  quantity: number;
}