export interface Product {
  id: number;
  marcaId: number;
  nombre: string;
  precioVenta: number;
  sku: string;
  imagenes: ProductImage[];
}

export interface ProductImage {
  id: number;
  productoId: number;
  imagenUrl: string;
}