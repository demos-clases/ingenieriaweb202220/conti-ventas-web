export enum PAGE_STATE {
  NOTHING = 'nothing',
  LOADING = 'loading',
  SUCCESS = 'success',
  ERROR = 'error',
}