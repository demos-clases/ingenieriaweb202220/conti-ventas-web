import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Product } from '../data/models/product.model';


@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient) { }

  products(): Observable<Product[]> {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'bearer token.nekot');

    return this.http.get<Product[]>(
      `${environment.storeApiUrl}/api/v1/products`, {headers: headers});
  }
}
