import { Injectable } from '@angular/core';
import { ProductCarItem } from '../data/models/productCar.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { Product } from '../data/models/product.model';
@Injectable({
  providedIn: 'root'
})
export class ProductCarService {
  list: ProductCarItem[] = [];
  listSubject = new BehaviorSubject<ProductCarItem[]>(this.list);

  constructor() {
    const jsonData = localStorage.getItem('productCarItems');
    if (jsonData) {
      this.list = JSON.parse(jsonData);
      this.listSubject.next(this.list);
    } else {
      localStorage.setItem(
        'productCarItems', JSON.stringify(this.list));
    }
  }

  getItems(): Observable<ProductCarItem[]> {
    return this.listSubject.asObservable();
  }

  addProduct(product: Product) {
    const exist = this.list
      .find((item) => item.product.id === product.id);
    if (exist) {
      exist.quantity += 1;
    }
    else {
      const item: ProductCarItem = {
        product, 
        quantity: 1
      }
      this.list.push(item);
    }
    this.updateItems();
  }

  removeProduct(productId: number) {
    for(let i = 0; i < this.list.length; i++) {
      if (this.list[i].product.id === productId) {
        // borramos!!!!!
        this.list.splice(i, 1);
        this.updateItems();
        break;
      }
    }
  }
  
  changeQuantity(productId: number, changedQuantity: number) {
    for(let i = 0; i < this.list.length; i++) {
      if (this.list[i].product.id === productId) {
        // actualizamos!!!!!
        this.list[i].quantity = changedQuantity;
        this.updateItems();
        break;
      }
    }
  }

  updateItems() {
    localStorage.setItem(
      'productCarItems', JSON.stringify(this.list));
    this.listSubject.next(this.list);
  }
}
