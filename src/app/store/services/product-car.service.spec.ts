import { TestBed } from '@angular/core/testing';

import { ProductCarService } from './product-car.service';

describe('ProductCarService', () => {
  let service: ProductCarService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductCarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
