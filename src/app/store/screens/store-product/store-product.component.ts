import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-store-product',
  templateUrl: './store-product.component.html',
  styleUrls: ['./store-product.component.sass']
})
export class StoreProductComponent implements OnInit {
  name?: string;
  product?: any;
  constructor() { }

  ngOnInit(): void {
  }

}
