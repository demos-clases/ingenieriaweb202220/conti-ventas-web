import { Component, OnInit } from '@angular/core';
import { PAGE_STATE } from '../../data/emuns/state.enum';
import { ProductsService } from '../../services/products.service';

@Component({
  selector: 'app-store-home',
  templateUrl: './store-home.component.html',
  styleUrls: ['./store-home.component.sass']
})
export class StoreHomeComponent implements OnInit {
  name?: string;
  pageState: PAGE_STATE = PAGE_STATE.LOADING;
  states = PAGE_STATE;
  products: any[] = [];
  constructor(
    private productService: ProductsService,
  ) { }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    this.pageState = PAGE_STATE.LOADING;
    this.productService.products().subscribe({
      next: (products) => {
        this.products = products;
        this.pageState = PAGE_STATE.SUCCESS;
      },
      error: (err) => {
        console.log(err);
        this.pageState = PAGE_STATE.ERROR;
      }
    });
  }
}

