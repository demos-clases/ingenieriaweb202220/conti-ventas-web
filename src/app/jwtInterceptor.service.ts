import { HttpEvent, HttpHandler, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable()
export class JwtInterceptorService {

    intercept(
      request: HttpRequest<any>, next: HttpHandler
    ): Observable<HttpEvent<any>> {
      const token = localStorage.getItem('authorization-token') ?? 'none.none';
      request = request.clone({
        setHeaders: {
          Authorization: `bearer ${token}`,
        }
      });
      return next.handle(request);
    }
}